package org.badsquad.filemanager.core;

import org.badsquad.filemanager.core.event.Event;
import org.badsquad.filemanager.core.event.ResponseEvent;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Класс предназначен для извлечения файлов из архива
 */
public class UnarchiveTask extends Task {
    private File input;
    private File output;

    /**
     * Конструктор для инициализация объекта
     */
    public UnarchiveTask(File input, File output, TaskListener listener) {
        super(listener);
        this.input = input;
        this.output = output;
    }

    /**
     * Метод выполняет извлечение файлов из архива
     * При начале извлечения оповещает слушателя событием Event.UNARCHIVE_START
     * В конце извлечения оповещает слушателя событием Event.UNARCHIVE_END об окончании извлечения
     * В случае возникновения ошибок оповещает слушателя:
     * IO_EXCEPTION - при ошибке ввода/вывода
     * FILE_EXISTS - при существовании конечного файла
     * NOT_EXISTS - при отсутствии исходного файла
     */
    @Override
    public void execute() {
        if (!input.exists()) {
            sendInfoEvent(Event.NOT_EXISTS, new Object[]{input, output});
            return;
        }
        
        output.mkdirs();
        try (ZipInputStream zip = new ZipInputStream(new FileInputStream(input))) {
            ZipEntry entry;
            while ((entry = zip.getNextEntry()) != null) {
                File file = new File(output, entry.getName());
                sendInfoEvent(Event.UNARCHIVING_START, new Object[]{entry, file});
                if(file.exists()) {
                	ResponseEvent response = sendWarnEvent(Event.FILE_EXISTS, new Object[]{entry, file});
	                if (response == ResponseEvent.DO_NEXT_STEP) {
	                    continue;
	                } else if (response == ResponseEvent.CANCEL_TASK) {
	                    return;
	                }
                }
                if (entry.isDirectory()) {
                    file.mkdirs();
                    continue;
                }
                try (FileOutputStream out = new FileOutputStream(file)) {
                    byte[] buffer = new byte[8192];
                    int bytesCopied;
                    while ((bytesCopied = zip.read(buffer)) > 0) {
                        out.write(buffer, 0, bytesCopied);
                    }
                } catch (IOException e) {
                    ResponseEvent res = sendWarnEvent(Event.IO_EXCEPTION, new Object[]{ e});
                    if (res == ResponseEvent.CANCEL_TASK) {
                        return;
                    }
                }
                sendInfoEvent(Event.UNARCHIVING_END, new Object[]{entry, file});
            }
        } catch (IOException e) {
            sendWarnEvent(Event.IO_EXCEPTION, new Object[]{e});
        }
    }

    /**
     * Метод выводит информацию об объекте
     */
    @Override
    public String toString() {
        return "Разархивация файла";
    }
}
