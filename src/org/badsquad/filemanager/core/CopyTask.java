package org.badsquad.filemanager.core;

import org.badsquad.filemanager.core.event.Event;
import org.badsquad.filemanager.core.event.ResponseEvent;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Класс CopyTask предназначен для копирования одного или нескольких файлов,
 * папок включая их содержимое, в том числе и вложенные папки.
 *
 * @author Комиссарова М.Е., 16IT18K
 */

public class CopyTask extends Task {
    /*
     * Исходный файл
     */
    private File inputFile;
    /*
     * Конечный файл
     */
    private File outFile;
    /*
     * Массив исходных файлов
     */
    private File[] arrayInputFile;

    /**
     * Конструктор для копирования одного файла
     *
     * @param listener  слушатель событий
     * @param inputFile исходный файл
     * @param outFile   конечный файл
     */
    public CopyTask(File inputFile, File outFile, TaskListener listener) {
        super(listener);
        this.inputFile = inputFile;
        this.outFile = outFile;
    }

    /**
     * Конструктор для копирования множества файлов
     *
     * @param array исходный файл
     * @param outFile конечный файл
     * @param listener слушатель событий
     */
    public CopyTask(File[] array, File outFile, TaskListener listener) {
        super(listener);
        this.arrayInputFile = array;
        this.outFile = outFile;
    }

    /**
     * Метод выполняет копирование:
     * Если объект предназначен для перемещения одного файла или папки,
     * то перемещает один файл или папку.
     * Если объект предназначен для перемещения массива,
     * то перемещает каждый файл из массива
     */
    @Override
    public void execute() {
        if (arrayInputFile == null) {
            copyFolder(inputFile, outFile);
        } else {
            outFile.mkdirs();
            for (File file : arrayInputFile) {
                if (!copyFolder(file, new File(outFile, file.getName()))) {
                    return;
                }
            }
        }
    }

    /**
     * Метод предназначен для копирования помимо файлов ещё и папок с их подпапками
     * При начале копирования оповещает слушателя событием Event.COPY_START
     * В конце копирования оповещает слушателя событием Event.COPY_END об окончании копирования
     * В случае возникновения ошибок оповещает слушателя:
     * SAME_FILE
     *
     * @param inputFile исходный файл
     * @param outputFile конечный файл
     * @return true во всех остальных случаях
     */
    private boolean copyFolder(File inputFile, File outputFile) {
        sendInfoEvent(Event.COPY_START, new Object[]{inputFile, outputFile});
        if (inputFile.equals(outputFile)) {
            sendInfoEvent(Event.SAME_FILE, new Object[]{inputFile, outputFile});
            return true;
        }
        if (inputFile.isDirectory()) {
            outputFile.mkdirs(); //создание новой директории
            File[] files = inputFile.listFiles();//масив файлов
            if (files != null) {
                for (File file : files) {
                    if (!copyFolder(file, new File(outputFile, file.getName()))) {
                        return false;
                    }
                }
            }
        } else {
            if (!copyFile(inputFile, outputFile)) {
                return false;
            }
        }
        sendInfoEvent(Event.COPY_END, new Object[]{inputFile, outputFile});
        return true;
    }

    /**
     * Метод предназначен для копирования помимо файлов ещё и папок с их подпапками
     * В случае возникновения ошибок оповещает слушателя:
     * IO_EXCEPTION - при ошибке ввода/вывода
     * FILE_EXISTS - при существовании конечного файла
     *
     * @param inputFile исходный файл
     * @param outputFile конечный файл
     * @return true во всех остальных случаях
     */
    private boolean copyFile(File inputFile, File outputFile) {
        if (outputFile.exists()) {//существует ли файл
            ResponseEvent response = sendWarnEvent(Event.FILE_EXISTS, new Object[]{inputFile, outputFile});
            switch (response) {
                case CANCEL_TASK:
                    return false;
                case DO_NEXT_STEP:
                    return true;
            }
        }

        if (inputFile.exists()) {
            try (FileInputStream fileInputStream = new FileInputStream(inputFile);
                 FileOutputStream fileOutputStream = new FileOutputStream(outputFile)) {
                byte[] array = new byte[8192];
                int count;
                while ((count = fileInputStream.read(array)) != -1) {
                    fileOutputStream.write(array, 0, count);
                }
            } catch (IOException e) {
                ResponseEvent response = sendWarnEvent(Event.IO_EXCEPTION, new Object[]{e});
                switch (response) {
                    case DO_NEXT_STEP:
                        return false;

                }
            }
        } else {
            sendInfoEvent(Event.NOT_EXISTS, new Object[]{inputFile, outputFile});
        }

        return true;
    }

    /**
     * Возвращает информацию о копировании файла inputFile в outFile
     *
     * @return inputFile outFile
     */
    @Override
    public String toString() {
        return "Копирование " + inputFile + " в " + outFile + ".";
    }
}
