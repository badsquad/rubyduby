package org.badsquad.filemanager.core;

import org.badsquad.filemanager.core.event.Event;
import org.badsquad.filemanager.core.event.ResponseEvent;

/**
 * Слушатель событий
 * 
 * @author Kirill Ispolnov
 */
public interface TaskListener {
    /**
     * Вызывается для оповещения о событии, требующем ответа пользователя
     * 
     * @param task Задача, в которой произошло событие
     * @param event Произошедшее событие
     * @param data Дополнительные данные, необходимые для уточнения информации о событии
     * @return ответ пользователя, представленный одним из элементов {@link ResponseEvent}
     */
	public ResponseEvent onWarnEvent(Task task, Event event, Object[] data);
	/**
	 * Вызывается для оповещения о произошедшем событии, не требующем ответа пользователя
	 * 
	 * @param task Задача, в которой произошло событие
	 * @param event Произошедшее событие
	 * @param data Дополнительные данные, необходимые для уточнения информации о событии
	 */
	public void onInfoEvent(Task task, Event event, Object[] data);
	
	/**
	 * Вызывается при оповещении о начале исполнения задачи
	 * 
	 * @param task задача, исполнение которой начато
	 */
	public void onTaskStart(Task task);
	/**
     * Вызывается при оповещении о завершении исполнения задачи
     * 
     * @param task задача, исполнение которой окончено
     */
	public void onTaskFinish(Task task);
}
