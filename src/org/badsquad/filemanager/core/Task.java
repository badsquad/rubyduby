package org.badsquad.filemanager.core;

import org.badsquad.filemanager.core.event.Event;
import org.badsquad.filemanager.core.event.ResponseEvent;

/**
 * Представляет некоторую задачу, взаимодействующую с некоторым слушателем событий
 * 
 * @author Kirill Ispolnov
 */
public abstract class Task implements Runnable {
	protected TaskListener listener;
	
	/**
	 * Конструктор добавлен для замещения конструктора по умолчвнию
	 * В привычном виде конструктор быть вызван не может, однако он необходим
	 * для инициализации объекта слушателем
	 * 
	 * @param listener слушатель событий
	 */
	public Task(TaskListener listener) {
		this.listener = listener;
	}
	
	/**
	 * Исполняет реализованный в методе {@link #execute()} функционал
	 * При этом перед началом исполнения слушатель оповещается о событии старта исполнения задачи
	 * По завершении исполнения метода {@link #execute()} слушатель также оповещается о событии завершения задачи 
	 */
	public final void run() {
		this.listener.onTaskStart(this);
		this.execute();
		this.listener.onTaskFinish(this);
	}
	
	/**
	 * Класс, наследующийся от данного класса должен реализовать в данном методе весь
	 * основной функционал 
	 */
	public abstract void execute();
	
	/**
	 * Оповещает слушателя о произошедшем событии, требующем ответа пользователя
	 * 
	 * @param event Произошедшее событие
	 * @param data Дополнительные данные, необходимые для уточнения информации о событии
	 * @return ответ пользователя, представленный одним из элементов {@link ResponseEvent}
	 */
	public ResponseEvent sendWarnEvent(Event event, Object[] data) {
		return listener.onWarnEvent(this, event, data);
	}
	
	/**
	 * Оповещает слушателя о произошедшем событии, не требующем ответа пользователя
     * 
	 * @param event Произошедшее событие
	 * @param data Дополнительные данные, необходимые для уточнения информации о событии
	 */
	public void sendInfoEvent(Event event, Object[] data) {
		listener.onInfoEvent(this, event, data);
	}
}
