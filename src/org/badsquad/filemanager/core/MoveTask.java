package org.badsquad.filemanager.core;

import org.badsquad.filemanager.core.event.Event;
import org.badsquad.filemanager.core.event.ResponseEvent;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Класс предназначен для перемещения одного или нескольких файлов,
 * папок включая их содержимое, в том числе и вложенные папки
 */
public class MoveTask extends Task {
    private File[] inputArray;
    private File input;
    private File output;

    /**
     * Конструктор для инициализация объекта
     */
    public MoveTask(File input, File output, TaskListener listener) {
        super(listener);
        this.input = input;
        this.output = output;
    }

    /**
     * Конструктор для инициализация массива
     */
    public MoveTask(File[] array, File output, TaskListener listener) {
        super(listener);
        this.inputArray = array;
        this.output = output;
    }


    /**
     * Метод выполняет перемещение:
     * Если объект предназначен для перемещения одного файла или папки,
     * то перемещает один файл или папку.
     * Если объект предназначен для перемещения массива,
     * то перемещает каждый файл из массива
     */
    @Override
    public void execute() {
        if (inputArray == null) {
            move(input, output);
        } else {
            for (File file : inputArray) {
                if(move(file, new File(output, file.getName())) == 0) {
                	break;
                }
            }
        }
    }

    /**
     * Метод выполняет перемещение файла
     * При начале перемещения оповещает слушателя событием Event.MOVE_START
     * В конце перемещения оповещает слушателя событием Event.MOVE_END об окончании перемещения
     * В случае возникновения ошибок оповещает слушателя:
     * IO_EXCEPTION - при ошибке ввода/вывода
     * FILE_EXISTS - при существовании конечного файла
     * NOT_EXISTS - при отсутствии исходного файла
     *
     * @param in  - исходный файл
     * @param out - конечный файл
     * @return true в остальных случаях
     */
    private int moveFile(File in, File out) {
        if (in.exists()) {
            if (out.exists()) {
                ResponseEvent response = sendWarnEvent(Event.FILE_EXISTS, new Object[]{in, out});
                if (response == ResponseEvent.DO_NEXT_STEP) {
                    return 1;
                } else if (response == ResponseEvent.CANCEL_TASK) {
                    return 0;
                }
            }
            out.getParentFile().mkdirs();
            try (FileInputStream fileOne = new FileInputStream(in);
                 FileOutputStream fileTwo = new FileOutputStream(out)) {
                byte[] buffer = new byte[8192];
                int bytesCopied;
                while ((bytesCopied = fileOne.read(buffer)) > 0) {
                    fileTwo.write(buffer, 0, bytesCopied);
                }
            } catch (IOException e) {
                ResponseEvent res = sendWarnEvent(Event.IO_EXCEPTION, new Object[]{ e});
                if (res == ResponseEvent.CANCEL_TASK) {
                    return 0;
                }
            }
        } else {
            sendInfoEvent(Event.NOT_EXISTS, new Object[]{in, out});
        }
        return 2;
    }

    /**
     * Метод выполняет перемещение папок и фалйов
     * При начале перемещения оповещает слушателя событием Event.MOVE_START
     * В конце перемещения оповещает слушателя событием Event.MOVE_END об окончании перемещения
     *
     * @param in  - исходныйфайл
     * @param out - конечный файл
     * @return true в остальных случаях
     */
    private int move(File in, File out) {
        sendInfoEvent(Event.MOVE_START, new Object[]{in, out});
        if (in.equals(out)){
            sendInfoEvent(Event.SAME_FILE, new Object[]{in, out});
            return 1;
        }
        int dir = 2;
        if (in.isDirectory()) {
            out.mkdirs();
            File[] files = in.listFiles();
            if (files != null) {
                for (File file : files) {
                    int can = move(file, new File(out, file.getName()));
                    if(can == 0) {
                    	return 0;
                    }
                    
                    if (can == 2) {
                        file.delete();
                    }
                }
            }
        } else {
            dir = moveFile(in, out);
        }
        if (dir == 2) {
            in.delete();
        }
        sendInfoEvent(Event.MOVE_END, new Object[]{in, out});
        return dir;
    }

    /**
     * Метод выводит информацию об объекте
     */
    @Override
    public String toString() {
        return "Перемещение файла";
    }
}

