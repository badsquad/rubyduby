package org.badsquad.filemanager.core;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.badsquad.filemanager.core.event.Event;
import org.badsquad.filemanager.core.event.ResponseEvent;

public class EncryptTask extends Task {
	private String password;
	private File source;
	private File target;
	
	public EncryptTask(File source, File target, String password, TaskListener listener) {
		super(listener);
		this.source = source;
		this.target = target;
		this.password = password;
	}

	@Override
	public void execute() {
		sendInfoEvent(Event.ENCRYPTION_START, new Object[]{ source, target });
		if(!source.exists()) {
			sendInfoEvent(Event.NOT_EXISTS, new Object[]{ source, target });
			return;
		}
		
		if(target.exists()) {
			ResponseEvent event = sendWarnEvent(Event.FILE_EXISTS, new Object[]{ source, target });
			if(event == ResponseEvent.CANCEL_TASK) {
				return;
			}
		}
		
		try(BufferedInputStream in = new BufferedInputStream(new FileInputStream(source));
			BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(target))) {
			
			Key key = new SecretKeySpec(Arrays.copyOf(password.getBytes(), 16), "AES");
			
	        Cipher cipher = Cipher.getInstance("AES");
	        cipher.init(Cipher.ENCRYPT_MODE, key);
	        
	        byte[] buffer = new byte[8192];
	        int count;
	        while((count = in.read(buffer)) != -1) {
	        	buffer = Arrays.copyOfRange(buffer, 0, count);
	        	byte[] encrypted = cipher.doFinal(buffer);
	        	out.write(encrypted);
	        }
		} catch(IOException ioe) {
			sendInfoEvent(Event.IO_EXCEPTION, new Object[]{ ioe });
		} catch(GeneralSecurityException gse) {
			sendInfoEvent(Event.SECURE_EXCEPTION, new Object[]{ gse });
		}
		
		sendInfoEvent(Event.ENCRYPTION_END, new Object[]{ source, target });
	}
}
