package org.badsquad.filemanager.core;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Синглтон, позволяющий исполнить задачу в пуле потоков
 * 
 * @author Kirill Ispolnov
 */
public class TaskExecutor {
	public static final int THREADS_COUNT = 8;
	private static TaskExecutor instance;
	private ExecutorService taskExecutor;
	
	/**
	 * Возвращает ссылку на единственный экземляр данного класса
	 * 
	 * @return ссылку на единственный экземляр данного класса
	 */
	public static TaskExecutor getInstance() {
		if(instance == null) {
			instance = new TaskExecutor();
			instance.taskExecutor = Executors.newFixedThreadPool(THREADS_COUNT);
		}
		return instance;
	}
	
	private TaskExecutor() {}
	
	/**
	 * Позволяет добавить задачу в очередь на выполнение в пуле
	 * 
	 * @param task задача для добавления в очередь
	 */
	public void execute(Task task) {
		taskExecutor.execute(task);
	}
	
	public void executeUI(Runnable task) {
		taskExecutor.execute(task);
	}
}
