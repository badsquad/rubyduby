package org.badsquad.filemanager.core;

import org.badsquad.filemanager.core.event.Event;

import java.io.File;
import java.io.IOException;

/**
 * Класс предназначен для переименования файлов
 */
public class RenameTask extends Task {
    private File file;
    private File renameFile;

    /**
     * Конструктор для инициализация объекта
     * @param file файл
     * @param fileName имя файла
     * @param listener слушатель событий
     */
    public RenameTask(File file, String fileName, TaskListener listener) {
        super(listener);
        this.file = file;
        this.renameFile = new File(file.getParentFile(), fileName);
    }

    /**
     * Метод переименовывает файл
     * В случае возникновения ошибки оповещает слушателя:
     * SAME_FILE - когда исходный и конечный файл одинаковые
     * INVALID_NAME - не корректное имя
     * DIFFERENT_DIR - при попытке переместить файл с помощью переименования
     * RENAME_FAILED - при ошибке переименования
     */
    @Override
    public void execute() {
        if (file.equals(renameFile)) {
            sendInfoEvent(Event.SAME_FILE, new Object[]{file, renameFile});
        }
        try {
            renameFile.getCanonicalPath();

        } catch (IOException ioe) {
            sendInfoEvent(Event.INVALID_NAME, new Object[]{file, renameFile});
            return;
        }

        if (file.getParentFile().equals(renameFile.getParent())) {
            sendInfoEvent(Event.DIFFERENT_DIR, new Object[]{file, renameFile});
            return;
        }

        if (!file.renameTo(renameFile)) {
            sendInfoEvent(Event.RENAME_FAILED, new Object[]{file, renameFile});
        }
    }
    
    @Override
    public String toString() {
    	return "Переименование";
    }
}


