package org.badsquad.filemanager.core;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.badsquad.filemanager.core.event.Event;
import org.badsquad.filemanager.core.event.ResponseEvent;

public class DecryptTask extends Task {
	private String password;
	private File encrypted;
	private File decrypted;
	
	public DecryptTask(File encrypted, File decrypted, String password, TaskListener listener) {
		super(listener);
		this.encrypted = encrypted;
		this.decrypted = decrypted;
		this.password = password;
	}

	@Override
	public void execute() {
		sendInfoEvent(Event.DECRYPTION_START, new Object[]{ encrypted, decrypted });
		if(!encrypted.exists()) {
			sendInfoEvent(Event.NOT_EXISTS, new Object[]{ encrypted, decrypted });
			return;
		}
		
		if(decrypted.exists()) {
			ResponseEvent event = sendWarnEvent(Event.FILE_EXISTS, new Object[]{ encrypted, decrypted });
			if(event == ResponseEvent.CANCEL_TASK) {
				return;
			}
		}
		
		try(BufferedInputStream in = new BufferedInputStream(new FileInputStream(encrypted));
			BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(decrypted))) {
			
	        Cipher cipher = Cipher.getInstance("AES");
	        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(Arrays.copyOf(password.getBytes(), 16), "AES"));
	        
	        byte[] buffer = new byte[8192];
	        int count;
	        while((count = in.read(buffer)) != -1) {
	        	buffer = Arrays.copyOfRange(buffer, 0, count);
	        	byte[] encrypted = cipher.doFinal(buffer);
	        	out.write(encrypted);
	        }
		} catch(IOException ioe) {
			sendInfoEvent(Event.IO_EXCEPTION, new Object[]{ ioe });
		} catch(GeneralSecurityException gse) {
			sendInfoEvent(Event.SECURE_EXCEPTION, new Object[]{ gse });
		}
		
		sendInfoEvent(Event.DECRYPTION_END, new Object[]{ encrypted, decrypted });
	}

}
