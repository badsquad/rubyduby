package org.badsquad.filemanager.core;

import java.io.File;
import java.util.List;

/**
 * Представляет обобщенный доступ к операциям с файловой системой
 * 
 * @author Kirill Ispolnov
 */
public final class FileManager {
	private TaskExecutor executor;
	private TaskListener listener;
	
	/**
	 * Инициализурует объект полученным в аргументы слушателем и полученным с помощью {@link TaskExecutor#getInstance()} экземляром TaskExecutor
	 * 
	 * @param listener
	 */
	public FileManager(TaskListener listener) {
		this.executor = TaskExecutor.getInstance();
		this.listener = listener;
	}
	
	/**
	 * Выполняет копирование файлов в указанный каталог
	 * 
	 * @param files список файлов
	 * @param target указанный каталог
	 */
	public void copy(List<File> files, File target) {
		Task copy;
		if(files.size() == 1) {
			copy = new CopyTask(files.get(0), target, listener);
		} else {
			copy = new CopyTask(files.toArray(new File[0]), target, listener);
		}
		executor.execute(copy);
	}
		
	/**
     * Выполняет перемещение файлов в указанный каталог
     * 
     * @param files список файлов
     * @param target указанный каталог
     */
	public void move(List<File> files, File target) {
		Task task;
		if(files.size() == 1) {
			task = new MoveTask(files.get(0), target, listener);
		} else {
			task = new MoveTask(files.toArray(new File[0]), target, listener);
		}
		executor.execute(task);
	}
	
	/**
     * Выполняет удаление файлов
     * 
     * @param files список файлов
     * @param useSystemTrash флаг, позволяющий указать нужно ли пытаться удалить файлы в системную корзину
	 */
	public void delete(List<File> files, boolean useSystemTrash) {
		Task task;
		if(files.size() == 1) {
			task = new DeleteTask(files.get(0), useSystemTrash, listener);
		} else {
			task = new DeleteTask(files.toArray(new File[0]), useSystemTrash, listener);
		}
		executor.execute(task);
	}
	
	/**
	 * Переименовывает файл
	 * 
	 * @param source исходный файл
	 * @param name новое имя
	 */
	public void rename(File source, String name) {
		executor.execute(new RenameTask(source, name, listener));
	}
	
	/**
	 * Добавляет файлы в архив
	 * 
	 * @param files список файлов
	 * @param zipArchive файл архива
	 */
	public void archive(List<File> files, File zipArchive) {
		Task task;
		if(files.size() == 1) {
			task = new ArchiveTask(files.get(0), zipArchive, listener);
		} else {
			task = new ArchiveTask(files.toArray(new File[0]), zipArchive, listener);
		}
		executor.execute(task);
	}
	
	/**
	 * Разархивирует файлы из архива
	 * 
	 * @param zipArchive файл архива
	 * @param dir папка, в которую следует распаковать файлы из архива
	 */
	public void unarchive(File zipArchive, File dir) {
		executor.execute(new UnarchiveTask(zipArchive, dir, listener));
	}
	
	/**
	 * Выполняет шифрование файла
	 * 
	 * @param origin исходный файл
	 * @param encrypted конечный файл, в котором будет сохранено зашифрованное содержимое
	 * @param password пароль для шифрования
	 */
	public void encrypt(File origin, File encrypted, String password) {
		executor.execute(new EncryptTask(origin, encrypted, password, listener));
	}
	
	/**
     * Выполняет шифрование файла
     * 
     * @param encrypted зашифрованный файл
     * @param decrypted исходный файл, в котором будет сохранено расшифрованное содержимое
     * @param password пароль для расшифрования
     */
	public void decrypt(File encrypted, File decrypted, String password) {
		executor.execute(new DecryptTask(encrypted, decrypted, password, listener));
	}
	
	/**
	 * Выполняет 
	 * 
	 * @param root папка, в которой производится поиск
	 * @param query имя искомого файла
	 * @param isRegex флаг, обозначающий является ли параметр query регулярным выражением
	 */
	public void search(File root, String query, boolean isRegex) {
		executor.execute(new SearchTask(root, query, isRegex, listener));
	}
}
