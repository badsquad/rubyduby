package org.badsquad.filemanager.core;

import java.io.File;
import java.util.ArrayList;

import org.badsquad.filemanager.core.event.Event;

public class SearchTask extends Task {
	private String namePattern;
	private File root;
	private boolean canSearch = true;
	
	public SearchTask(File root, String query, boolean isRegexp, TaskListener listener) {
		super(listener);
		if(!isRegexp) {
			query = "^" + query + "$";
		}
		this.namePattern = query;
		this.root = root;
	}
	
	@Override
	public void execute() {
		search(root);
	}

	private void search(File directory) {
		sendInfoEvent(Event.SEARCH_START, new Object[]{ directory, namePattern });
		ArrayList<File> results = new ArrayList<File>();
		
		File[] files = directory.listFiles();
		if(files != null) {
			for(File file : files) {
				if(file.getName().matches(namePattern)) {
					results.add(file);
				}
				
				if(file.isDirectory()) {
					search(file);
				} 
			}
		}
		
		sendInfoEvent(Event.SEARCH_END, new Object[]{ directory, namePattern, results });
	}
}
