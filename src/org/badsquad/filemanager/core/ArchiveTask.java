package org.badsquad.filemanager.core;

import org.badsquad.filemanager.core.event.Event;
import org.badsquad.filemanager.core.event.ResponseEvent;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Класс ArchiveTask предназначен для архивирования одного или нескольких файлов,
 * папок включая их содержимое, в том числе и вложенные папки.
 *
 * @author Комиссарова М.Е., 16IT18K
 */

public class ArchiveTask extends Task {
    /*
     * Массив исходных файлов
     */
    private File[] arrayInputFile;
    /*
     * Исходный файл
     */
    private File inputFile;
    /*
     * Конечный файл
     */
    private File outFile;

    /**
     * Конструктор для архивации файла
     *
     * @param inputFile
     * @param outFile
     * @param listener
     */
    public ArchiveTask(File inputFile, File outFile, TaskListener listener) {
        super(listener);
        this.inputFile = inputFile;
        this.outFile = outFile;
    }

    /**
     * Конструктор для архивации множества файлов
     *
     * @param array исходный файл
     * @param outFile конечный файл
     * @param listener слушатель событий
     **/
    public ArchiveTask(File[] array, File outFile, TaskListener listener) {
        super(listener);
        this.arrayInputFile = array;
        this.outFile = outFile;
    }

    /**
     * Метод выполняет архивирование файла или папки
     * В случае возникновения ошибок оповещает слушателя:
     * IO_EXCEPTION - при ошибке ввода/вывода
     * FILE_EXISTS - при существовании конечного файла
     */
    @Override
    public void execute() {
    	if(outFile.exists()) {
            ResponseEvent responseEvent = sendWarnEvent(Event.FILE_EXISTS, new Object[]{inputFile, outFile});
	        if (responseEvent == ResponseEvent.CANCEL_TASK) {
	            return;
	        }
    	}
    	
        try (ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(outFile))) {
            if (arrayInputFile == null) {
                archiveFile(inputFile, "", zipOutputStream);
            } else {
                for (File file : arrayInputFile) {
                    if (!archiveFile(file, "", zipOutputStream)) {
                        return;
                    }
                }
            }
        } catch (IOException e) {
            sendInfoEvent(Event.IO_EXCEPTION, new Object[]{e});
        }
    }

    /**
     * Метод выполняет архивирования файла
     * При начале архивирования оповещает слушателя событием ARCHIVING_START
     * В конце архивирования оповещает слушателя событием ARCHIVING_END об окончании архивирования
     * В случае возникновения ошибок оповещает слушателя:
     * IO_EXCEPTION - при ошибке ввода/вывода
     * NOT_EXISTS - при отсутствии исходного файла
     *
     * @param inputFile исходный файл
     * @param path относительный путь
     * @param zipOutputStream поток отвечающий за архивирование файла
     * @return true во всех остальных случаях
     */
    private boolean archiveFile(File inputFile, String path, ZipOutputStream zipOutputStream) {
        sendInfoEvent(Event.ARCHIVING_START, new Object[]{inputFile, outFile});
        if (!inputFile.exists()) {
            ResponseEvent response = sendWarnEvent(Event.NOT_EXISTS, new Object[]{inputFile, outFile});
            switch (response) {
                case DO_NEXT_STEP:
                    return true;
                case CANCEL_TASK:
                    return false;
            }
        }
        if (inputFile.isDirectory()) {
            path += inputFile.getName() + File.separator;
            File[] array = inputFile.listFiles();
            if (array != null) {
                for (File file : array) {
                    if (!archiveFile(file, path, zipOutputStream)) {
                        return false;
                    }
                }
            }
        } else {
            try (FileInputStream fileInputStream = new FileInputStream(inputFile)) {
                ZipEntry entry = new ZipEntry(path + inputFile.getName());
                zipOutputStream.putNextEntry(entry);

                byte[] array = new byte[8192];
                int count;
                while ((count = fileInputStream.read(array)) != -1) {
                    zipOutputStream.write(array, 0, count);
                }

                zipOutputStream.closeEntry();

            } catch (IOException e) {
                ResponseEvent response = sendWarnEvent(Event.IO_EXCEPTION, new Object[]{inputFile, outFile, e});
                switch (response) {
                    case CANCEL_TASK:
                        return false;
                }
            }
        }

        sendInfoEvent(Event.ARCHIVING_END, new Object[]{inputFile, outFile});

        return true;
    }

    /**
     * Возвращает информацию о архивации файла inputFile в outFile
     *
     * @return inputFile outFile
     */
    @Override
    public String toString() {
        return "Архивирование " + inputFile + " в " + outFile + ".";
    }
}
