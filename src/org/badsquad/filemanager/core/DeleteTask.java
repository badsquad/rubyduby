package org.badsquad.filemanager.core;


import com.sun.jna.platform.FileUtils;
import org.badsquad.filemanager.core.event.Event;

import java.io.File;
import java.io.IOException;

/**
 * В классе DeleteTask реализованы методы:
 * delete выполняет удаление файла
 * deleteToBasket выполняет перемещение файла в корзину
 * toString выводит информацию о удалении файла
 * execute удаляет файлы,
 * а также: если файл не найден оповещает пользователя
 */

public class DeleteTask extends Task {
    private File[] deleteArray;
    private File file;
    private boolean sendbasket;

    /**
     * Конструктор для инициализация объекта
     * @param file файл
     * @param sendbasket флаг указываюший куда будет происходить удаление
     *                   true выполняется перемешение в корзину
     *                   false выполняется удаление
     * @param listener слушатель событий
     */

    public DeleteTask(File file, boolean sendbasket, TaskListener listener) {
        super(listener);
        this.file = file;
        this.sendbasket = sendbasket;


    }
    /**
     * Конструктор для инициализация объекта
     * @param deleteArray массив файлов
     * @param sendbasket флаг указываюший куда будет происходить удаление
     *                   true выполняется перемешение в корзину
     *                   false выполняется удаление
     * @param listener слушатель событий
     */

    public DeleteTask(File[] deleteArray, boolean sendbasket, TaskListener listener) {
        super(listener);
        this.deleteArray = deleteArray;
        this.sendbasket = sendbasket;

    }

    /**
     * Метод  удаляет файлы и если файл не найден оповещает слушателя
     * NOT_EXISTS-файла не сушествует
     */

    @Override
    public void execute() {
        if (deleteArray == null) {
            delete(file);
        } else {
            for (File aDeleteArray : deleteArray) {
                delete(aDeleteArray);
            }
        }
    }

    private void delete(File file) {
        if (!file.exists()) {
            sendInfoEvent(Event.NOT_EXISTS, new Object[]{file});
            return;
        }
        if (sendbasket) {
            deleteToBasket((new File[]{file}));
        } else {
            deleteHard(file);
        }
    }

    /**
     * Метод  выполняет удаление файла
     * При начале удаления каждого файла оповещает слушателя событием:
     * DELETE_START - начало удаления
     * DELETE_END - удаление закончено
     */

    private void deleteHard(File dir) {
        sendInfoEvent(Event.DELETE_START, new Object[]{file});
        if (dir.isDirectory()) {
            File[] children = dir.listFiles();
            if (children != null) {
                for (File child : children) {
                    delete(child);

                }
            }
            dir.delete();
        } else {
            dir.delete();
        }
        sendInfoEvent(Event.DELETE_END, new Object[]{file});
    }

    /**
     * Метод  выполняет перемещение файла в корзину
     * IO_EXCEPTION-ошибка ввода/вывода
     */

    private void deleteToBasket(File[] deleteArray) {
        FileUtils fileUtils = FileUtils.getInstance();
        if (fileUtils.hasTrash()) {
            try {
                fileUtils.moveToTrash(deleteArray);
            } catch (IOException ioe) {
                sendWarnEvent(Event.IO_EXCEPTION, new Object[]{file, ioe});
            }
        }
    }

    /**
     * Метод выводит информацию о удалении файла
     */

    public String toString() {
        if (!sendbasket) {
            return "Удаление " + file;
        } else {
            return "Перемещение в корзину " + file;
        }
    }
}