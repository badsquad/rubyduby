package org.badsquad.filemanager.core.event;

/**
 * Представляет возможные варианты ответа пользователя
 * 
 * @author Kirill Ispolnov
 */
public enum ResponseEvent {
	DO_NEXT_STEP, /* пропустить текущий шаг */
	IGNORE_EVENT, /* пропустить событие (игнорировать) */
	CANCEL_TASK   /* отменить задачу */
}
