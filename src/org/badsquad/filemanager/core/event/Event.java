package org.badsquad.filemanager.core.event;

/**
 * Представляет возможные события для задач
 * 
 * @author Kirill Ispolnov
 */
public enum Event {
	FILE_EXISTS, NOT_EXISTS, IO_EXCEPTION,  	 /* all */
	DELETE_START, DELETE_END, 				 	 /* for delete task */
	COPY_START, COPY_END,	  				 	 /* for copytask */
	MOVE_START, MOVE_END,	  					 /* for movetask */
	ARCHIVING_START, ARCHIVING_END,			 	 /* for archivetask */
	UNARCHIVING_START, UNARCHIVING_END,  	     /* for unarchivetask */
	ENCRYPTION_START, ENCRYPTION_END, 
	SECURE_EXCEPTION,			 				 /* for encrypttask */
	DECRYPTION_START, DECRYPTION_END,			 /* for decrypttask */
	SAME_FILE, 				  					 /* for renametask, copytask, movetask */
	INVALID_NAME, RENAME_FAILED, DIFFERENT_DIR,  /* for renametask */
	SEARCH_START, SEARCH_END	  	  	  		 /* for searchtask */
}
