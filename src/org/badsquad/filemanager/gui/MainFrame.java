package org.badsquad.filemanager.gui;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import org.badsquad.filemanager.core.FileManager;
import org.badsquad.filemanager.core.SearchTask;
import org.badsquad.filemanager.core.Task;
import org.badsquad.filemanager.core.TaskListener;
import org.badsquad.filemanager.core.event.Event;
import org.badsquad.filemanager.core.event.ResponseEvent;
import org.badsquad.filemanager.gui.views.DirectoryView;
import org.badsquad.filemanager.gui.views.ToolsPanel;
import org.badsquad.filemanager.utils.DirectoryWatcher;

import com.sun.jna.platform.FileUtils;

/**
 * Класс представляющий главное окно
 * 
 * @author Kirill Ispolnov
 */
public class MainFrame extends JFrame implements TaskListener, GUIListener {
	private static final long serialVersionUID = -6868459698639424260L;

	//текущая папка
	private File currentDirectory;
	//виджет, содержащий список файлов в текущей папке
	private DirectoryView directoryView;
	//виджет, содержащий элементы управления для взаимодействия с файловой структурой
	private ToolsPanel toolsPanel;
	private FileManager manager;	
	
	//выбранные пользователем файлы
	private ArrayList<File> selectedFiles = new ArrayList<>();
	//результаты поиска
	private ArrayList<File> searchResults = new ArrayList<>();
	private HashMap<Object, ResponseEvent> responses = new HashMap<>();
	//таблица задач и их окон
	private HashMap<Task, TaskFrame> taskFrames = new HashMap<>();
	
	//наблюдатель за папкой
	private DirectoryWatcher watcher;
	
	public MainFrame() {
		this.directoryView = new DirectoryView(this);
		
		this.toolsPanel = new ToolsPanel(this);
		this.manager = new FileManager(this);
		
		add(toolsPanel, BorderLayout.NORTH);
		add(directoryView, BorderLayout.CENTER);
		
		watcher = new DirectoryWatcher(this);
		watcher.start();
		onFileClick(null);
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(screenSize.width / 4, screenSize.height / 5, screenSize.width / 2, screenSize.height * 3 / 5);
	}
	
	private File chooseFile(String extension, String description) {
		JFileChooser fc = new JFileChooser();
		fc.setMultiSelectionEnabled(false);
		fc.setCurrentDirectory(currentDirectory);
		if(extension != null) {
			fc.setFileFilter(new FileFilter() {
				@Override
				public boolean accept(File file) {
					return file.getName().endsWith(extension);
				}
	
				@Override
				public String getDescription() {
					return description;
				}
			});
		}
		fc.showOpenDialog(this);
		return fc.getSelectedFile();
	}
	
	private File chooseDirectory() {
		JFileChooser fc = new JFileChooser();
		fc.setMultiSelectionEnabled(false);
		fc.setCurrentDirectory(currentDirectory);
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fc.showOpenDialog(this);
		return fc.getSelectedFile();
	}
	
	private boolean isOnlyFiles() {
		for(File file : selectedFiles) {
			if(file.isDirectory()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Вызывается при нажатии на элемент управления
	 */
	@Override
	public void onToolClick(Tool tool) {
		if(tool == Tool.BACK) {
			onFileClick(currentDirectory.getParentFile());
			return;
		}
		
		if(tool == Tool.CHANGE_VIEW) {
			directoryView.toggleViewMode();
			return;
		}
	
		if(tool == Tool.MKDIR) {
			String name = JOptionPane.showInputDialog("Введите имя новой папки: ");
			if(name != null) {
				new File(currentDirectory, name).mkdir();
			}
			return;
		}
	
		
		if(tool != Tool.SEARCH && selectedFiles.size() == 0) {
			JOptionPane.showMessageDialog(this, "Сначала выберите хотя бы один файл");
			return;
		}

		File file;
		switch(tool) {
			case ARCHIVE: {
				file = chooseFile(".zip", "Архивы");
				if(file != null) {
					manager.archive(selectedFiles, file);
				}
				break;
			}
			case COPY: {
				if(selectedFiles.size() == 1 && selectedFiles.get(0).isFile()) 
					file = chooseFile(null, null);
				else
					file = chooseDirectory();
				
				if(file != null) {
					manager.copy(selectedFiles, file);
				}
				break;
			}
			case ENCRYPT: {
				if(!isOnlyFiles()){
					JOptionPane.showMessageDialog(this, "Шифровывать папки нельзя");
					return;
				}
				
				if(selectedFiles.size() > 1) {
					JOptionPane.showMessageDialog(this, "Выберите 1 файл");
					return;
				}
				
				file = chooseFile(null, null);
				if(file != null) {
					String password = JOptionPane.showInputDialog("Введите пароль (не менее 16 символов): ");
					manager.encrypt(selectedFiles.get(0), file, password);
				}
				
				break;
			}
			case DECRYPT: {
				if(!isOnlyFiles()){
					JOptionPane.showMessageDialog(this, "Расшифровывать папки нельзя");
					return;
				}
				
				if(selectedFiles.size() > 1) {
					JOptionPane.showMessageDialog(this, "Выберите 1 файл");
					return;
				}
				
				file = chooseFile(null, null);
				if(file != null) {
					String password = JOptionPane.showInputDialog("Введите пароль (не менее 16 символов): ");
					manager.decrypt(selectedFiles.get(0), file, password);
				}
				
				break;
			}
			case DELETE: {
				int flag = JOptionPane.showConfirmDialog(this, "Вы уверены, что хотите удалить файл?", "Подтверждение", JOptionPane.YES_NO_OPTION);

				if(flag == JOptionPane.NO_OPTION) {
					return;
				}
				
				flag = JOptionPane.NO_OPTION;
				
				if(FileUtils.getInstance().hasTrash()) {
					flag = JOptionPane.showConfirmDialog(this, "Удалить файл безвозвратно(Yes) или в Корзину(No)?", "Подтверждение", JOptionPane.YES_NO_CANCEL_OPTION);
				}
				
				if(flag == JOptionPane.CANCEL_OPTION) {
					return;
				}
				
				manager.delete(selectedFiles, flag == JOptionPane.NO_OPTION);
				
				break;
			}
			case MOVE: {
				if(selectedFiles.size() == 1) 
					file = chooseFile(null, null);
				else
					file = chooseDirectory();
				
				if(file != null) {
					manager.move(selectedFiles, file);
				}
				break;
			}
			case RENAME: {
				if(selectedFiles.size() > 1) {
					JOptionPane.showMessageDialog(this, "Выберите не более 1 файла");
				}
				
				file = selectedFiles.get(0);
				String name = JOptionPane.showInputDialog("Введите новое имя файла " + file + ": ");
				if(name != null)
					manager.rename(file, name);
				break;
			}
			case SEARCH: {
				String name = JOptionPane.showInputDialog("Введите имя искомого файла: ");
				if(name == null) {
					return;
				}
				
				manager.search(this.currentDirectory, name, false);
				break;
			}
			case UNARCHIVE: {
				if(selectedFiles.size() > 1) {
					JOptionPane.showMessageDialog(this, "Выберите один файл");
					return;
				}
				
				file = chooseDirectory();
				manager.unarchive(selectedFiles.get(0), file);
				break;
			}
			default: 
				break;
		}
	}

	/**
	 * Вызывается при двойном нажатии по иконке папки или файла
	 */
	@Override
	public void onFileClick(File file) {
		if(file != null && file.isFile()) {
			try {
				Desktop.getDesktop().open(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		
		if(file != null && !file.isDirectory() && !file.isFile()) {
			JOptionPane.showMessageDialog(this, "Вставьте диск " + file);
			return;
		}
		
		watcher.setDirectory(file);
		currentDirectory = file;
		selectedFiles.clear();
		directoryView.removeAllFiles();
		
		File[] files;
		if(file == null) {
			toolsPanel.disableTools();
			files = File.listRoots();
		} else {
			toolsPanel.enableTools();
			files = file.listFiles();
		}
		
		if(files != null) {
			for(File f : files) {
				directoryView.addFile(f);
			}
		}
	}

	@Override
	public void onFileSelect(File file) {
		selectedFiles.add(file);
	}

	@Override
	public void onFileDeselect(File file) {
		selectedFiles.remove(file);
	}
	
	@Override
	public ResponseEvent onWarnEvent(Task task, Event event, Object[] data) {
		TaskFrame frame = taskFrames.get(task);
		
		switch(event) {
			case FILE_EXISTS: { 
				frame.setMessage("Конечный файл " + data[1] + " существует. Перезаписать его?");
				break;
			}
			case NOT_EXISTS: {
				frame.setMessage("Исходный файл " + data[0] + " не найден");
				return ResponseEvent.CANCEL_TASK;
			}
			case IO_EXCEPTION: {
				frame.addEntry(data[0].toString());
				frame.setMessage("Произошла ошибка ввода-вывода");
				break;
			}
			default:
				break;
		}
		
		ResponseEvent rEvent = frame.getResponseEvent();
		responses.put(data[0], rEvent);
		System.err.println(rEvent);
		return rEvent;
	}

	@Override
	public void onInfoEvent(Task task, Event event, Object[] data) {
		TaskFrame frame = taskFrames.get(task);
		
		switch(event) {
			case INVALID_NAME: {
				frame.setMessage("Переименование не удалось: имя некорректно");
				break;
			}
			case RENAME_FAILED: {
				frame.setMessage("Переименование не удалось: ошибка переименования");
				break;
			}
			case SECURE_EXCEPTION: {
				frame.setMessage("Ошибка безопастности " + data[0]);
				break;
			}
			case SAME_FILE: {
				frame.setMessage("Исходный и конечный файл совпадают");
				break;
			}
			case NOT_EXISTS: {
				frame.setMessage("Исходный файл " + data[0] + " не найден");
				break;
			}
			case IO_EXCEPTION: {
				frame.addEntry(data[2].toString());
				frame.setMessage("Произошла ошибка ввода-вывода");
				break;
			}
			case DIFFERENT_DIR: {
				frame.setMessage("У нового имени указана папка-родитель, отличающая от папки-родителя исходного файла");
				break;
			}
			case ARCHIVING_START: {
				frame.addEntry("Архивация " + data[0]);
				break;
			}
			case ARCHIVING_END: {
				frame.addEntry("Файл " + data[0] + " добавлен в архив " + data[1]);
				break;
			}
			case COPY_START: {
				frame.addEntry("Копирование " + data[0] + " в " + data[1]);
				break;
			}
			case COPY_END: {
				ResponseEvent rEvent = responses.remove(data[0]);
				if(((File)data[0]).isFile() && (rEvent == null || rEvent == ResponseEvent.IGNORE_EVENT)) {
					frame.addEntry("Файл " + data[0] + " скопирован в " + data[1]);
				} 
				break;
			}
			case MOVE_START: {
				frame.addEntry("Перемещение " + data[0] + " в " + data[1]);
				break;
			}
			case MOVE_END: {
				ResponseEvent rEvent = responses.remove(data[0]);
				if(((File)data[0]).isFile() && (rEvent == null || rEvent == ResponseEvent.IGNORE_EVENT)) {
					frame.addEntry("Файл " + data[0] + " перемещен в " + data[1]);
				} 
				break;
			}
			case SEARCH_START: {
				frame.addEntry("Поиск в " + data[0]);
				break;
			}
			case SEARCH_END: {
				@SuppressWarnings("unchecked")
				List<File> results = (List<File>)data[2];
				if(results.size() > 0) {
					searchResults.addAll(results);
					frame.addEntry("Найденные файлы: " + results);
				} else {
					frame.addEntry("Не найдено файлов");
				}
				break;
			}
			case ENCRYPTION_START: {
				frame.addEntry("Файл " + data[0] + " зашифрован");
				break;
			}
			case ENCRYPTION_END: {
				frame.addEntry("Шифрование файла " + data[0]);
				break;
			}
			case DECRYPTION_START: {
				frame.addEntry("Расшифровка файла " + data[0]);
				break;
			}
			case DECRYPTION_END: {
				frame.addEntry("Файл " + data[0] + " расшифрован");
				break;
			}
			case DELETE_START: {
				frame.addEntry("Удаление " + data[0]);
				break;
			}
			case DELETE_END: {
				frame.addEntry("Файл " + data[0] + " удален");
				break;
			}
			case UNARCHIVING_START: {
				frame.setMessage("Извлечение " + data[0] + " в " + data[1]);
				break;
			}
			case UNARCHIVING_END: {
				frame.setMessage("Файл " + data[0] + " извлечен");
				break;
			}
			default:
				break;
		}
	}
	
	private int taskStart = 0;

	@Override
	public void onTaskStart(Task task) {
		taskStart++;
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		TaskFrame frame = new TaskFrame(true);
		frame.setTitle(task.toString());
		frame.setVisible(true);
		taskFrames.put(task, frame);
		
		if(task instanceof SearchTask) {
			frame.makeCanKill();
		}
	}

	@Override
	public void onTaskFinish(Task task) {
		taskStart--;
		if(taskStart == 0) {
			setDefaultCloseOperation(EXIT_ON_CLOSE);
		}
		
		TaskFrame frame = taskFrames.remove(task);
		frame.makeCanKill();
		
		if(task instanceof SearchTask) {
			frame.clearHistory();
			for(File file : searchResults) {
				frame.addEntry(file.getAbsolutePath());
			}
		}
	}

	@Override
	public void onFileCreated(File file) {
		System.err.println(file);
		this.directoryView.addFile(file);
	}

	@Override
	public void onFileDeleted(File file) {
		this.directoryView.removeFile(file);
	}
}
