package org.badsquad.filemanager.gui;

import java.io.File;

public interface GUIListener {
	public void onToolClick(Tool tool); //in tools panel
	public void onFileClick(File file); //in tree, in file view
	public void onFileSelect(File file); //in file view
	public void onFileDeselect(File file); //in file view
	public void onFileCreated(File file);
	public void onFileDeleted(File file);
}
