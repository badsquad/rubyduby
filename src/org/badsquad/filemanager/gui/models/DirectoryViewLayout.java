package org.badsquad.filemanager.gui.models;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;

import org.badsquad.filemanager.gui.Constants;

public class DirectoryViewLayout implements LayoutManager, Constants {
	private FileViewMode mode;

	public DirectoryViewLayout() {
		this.mode = FileViewMode.TABLE;
	}
	
	public void setMode(FileViewMode mode) {
		this.mode = mode;
	}
	
	public FileViewMode getMode() {
		return mode;
	}
	
	@Override
	public void addLayoutComponent(String name, Component comp) { }

	@Override
	public void removeLayoutComponent(Component comp) {	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		return new Dimension(300, 300);
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		return new Dimension(150, 150);
	}

	private int getWidth() {
		if(mode == FileViewMode.ICONS) {
			return ICON_FILE_VIEW_WIDTH;
		}
		return TABLE_FILE_VIEW_WIDTH;
	}
	
	private int getHeight() {
		if(mode == FileViewMode.ICONS) {
			return ICON_FILE_VIEW_HEIGHT;
		}
		return TABLE_FILE_VIEW_HEIGHT;
	}
	
	private int columnCount = 0;

	public int getColumnCount() {
		return columnCount;
	}
	
	@Override
	public void layoutContainer(Container target) {
		int pw = target.getWidth();
		
		int width = getWidth();
		int height = getHeight();
		
		int mc = pw / width;
		width = (int)((double)pw / (mc - 0.8));
		
		Insets insets = target.getInsets();
        int top = insets.top;
        int left = insets.left;
        
        int x = 0;
        int y = 0;
        for(int i = 0; i < target.getComponentCount(); i++) {
        	Component c = target.getComponent(i);
        	c.setSize(new Dimension(width, width));
        	
        	c.setBounds(left + x * (width + 10), top + y * (height + 10), width, height);
        	
        	if((++x + 1) * width > target.getWidth()) {
        		columnCount = Math.max(x, columnCount);
        		y++;
        		x = 0;
        	}
        }
        
        target.setPreferredSize(new Dimension(x * (width+15), (y+1) * (height+15)));
	}
}
