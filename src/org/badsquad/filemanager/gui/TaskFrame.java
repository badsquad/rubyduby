package org.badsquad.filemanager.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.badsquad.filemanager.core.event.ResponseEvent;
import org.badsquad.filemanager.utils.FontUtils;

public class TaskFrame extends JFrame implements ActionListener {
	private static final long serialVersionUID = -5729185183036001232L;

	private JLabel message;
	private JTextArea history;
	private JButton skip;
	private JButton cancel;
	private JButton ignore;
	private JButton ok;
	
	private boolean canSkip = false;
	
	private String originMessage;
	
	public TaskFrame(boolean canSkip) {
		this.canSkip = canSkip;
		
		JPanel content = new JPanel();
		content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));
		
		JScrollPane center = new JScrollPane();
		
		this.history = new JTextArea();
		history.setEditable(false);
		history.setBorder(null);
		history.setForeground(Color.DARK_GRAY);
		FontUtils.setFontSize(history, 1.3f);
		FontUtils.setFontStyle(history, Font.ITALIC);
		center.getViewport().add(history);
		content.add(center);
		
		this.message = new JLabel();
		FontUtils.setFontSize(message, 1.5f);
		FontUtils.setFontStyle(message, Font.PLAIN);
		content.add(message);
		add(content, BorderLayout.CENTER);
		
		JPanel controls = new JPanel();
		controls.setLayout(new FlowLayout(FlowLayout.RIGHT));
		
		this.skip = new JButton("Пропустить");
		skip.setEnabled(false);
		skip.addActionListener(this);
		controls.add(skip);
		
		this.ignore = new JButton("Продолжить");
		ignore.setEnabled(false);
		ignore.addActionListener(this);
		controls.add(ignore);
		
		this.cancel = new JButton("Отмена");
		cancel.setEnabled(false);
		cancel.addActionListener(this);
		controls.add(cancel);
		
		this.ok = new JButton("Завершить");
		ok.setEnabled(false);
		ok.addActionListener(this);
		controls.add(ok);
		
		add(controls, BorderLayout.SOUTH);
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(size.width / 3, size.height / 3, size.width / 3, size.height / 3);
		
	}
	
	public void setMessage(String message) {
		if(originMessage == null) {
			originMessage = message;
		}
		this.message.setText(message);
	}
	
	public void addEntry(String entry) {
		this.history.setText(this.history.getText() + entry + "\n"); 
	}
	
	public void clearHistory() {
		this.history.setText("");
	}
	
	public void restoreMessage() {
		message.setText(originMessage);
	}
	
	public void makeCanKill() {
		this.ok.setEnabled(true);
		this.setDefaultCloseOperation(HIDE_ON_CLOSE);
	}
	
	private Object response = null;

	public ResponseEvent getResponseEvent() {
		skip.setEnabled(canSkip);
		ignore.setEnabled(true);
		cancel.setEnabled(true);
		
		while(response == null) {
			setTitle("");
		}
		
		ResponseEvent event;
		if(response.equals(cancel)) {
			event = ResponseEvent.CANCEL_TASK;
		} else if(response.equals(ignore)) {
			event = ResponseEvent.IGNORE_EVENT;
		} else {
			event = ResponseEvent.DO_NEXT_STEP;
		}
		
		response = null;
		
		skip.setEnabled(false);
		ignore.setEnabled(false);
		cancel.setEnabled(false);
		message.setText("");
		
		return event;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(ok)) {
			dispose();
		} else {
			response = e.getSource();
		}
	}
}
