package org.badsquad.filemanager.gui.views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JButton;

public class ImageView extends JButton {
	private static final long serialVersionUID = -5122086221680650133L;

	private Image image;
	private Image whichDisabled;
	
	public ImageView(Image image, int width, int height) {
		if(image.getWidth(this) != width || image.getHeight(this) != height) {
			image = image.getScaledInstance(width, height, Image.SCALE_AREA_AVERAGING);
		}
		
		this.image = image;
		setPreferredSize(new Dimension(width, height));
		setBackground(new Color(238, 238, 238));
		setBorder(null);
	}
	
	public void setDisabled(Image image) {
		whichDisabled = image;
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(this.isEnabled()) {
			g.drawImage(image, 0, 0, null);
		} else if(whichDisabled != null) {
			g.drawImage(whichDisabled.getScaledInstance(getWidth(), getHeight(), Image.SCALE_AREA_AVERAGING), 0, 0, null);
		}
	}
}
