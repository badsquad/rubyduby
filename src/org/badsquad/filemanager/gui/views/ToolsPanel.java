package org.badsquad.filemanager.gui.views;

import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JPanel;

import org.badsquad.filemanager.gui.GUIListener;
import org.badsquad.filemanager.gui.Constants;
import org.badsquad.filemanager.gui.Tool;
import org.badsquad.filemanager.utils.Resources;

public class ToolsPanel extends JPanel implements ActionListener, Constants {
	private static final long serialVersionUID = 1154736420706997982L;
	private GUIListener clickListener;
	private ImageView backButton, searchButton, 
					  copyButton, moveButton, 
					  deleteButton, renameButton, 
					  archiveButton, unarchiveButton,
					  encryptButton, decryptButton;
	private ImageView viewButton, newFolderButton;
	
	public ToolsPanel(GUIListener listener) {
		this.clickListener = listener;
		setLayout(new FlowLayout(FlowLayout.LEFT));
		
		Image backImage = null;
		Image searchImage = null;
		Image copyImage = null;
		Image moveImage = null;
		Image renameImage = null;
		Image deleteImage = null;
		Image archiveImage = null;
		Image unarchiveImage = null;
		Image encryptImage = null;
		Image decryptImage = null;
		Image viewImage = null;
		Image newFolderImage = null;
		
		try {
			backImage = Resources.getImage("back.png");
			searchImage = Resources.getImage("search.png");
			copyImage = Resources.getImage("copy.png");
			moveImage = Resources.getImage("move.png");
			renameImage = Resources.getImage("rename.png");
			deleteImage = Resources.getImage("delete.png");
			archiveImage = Resources.getImage("archive.png");
			unarchiveImage = Resources.getImage("unarchive.png");
			encryptImage = Resources.getImage("encrypt.png");
			decryptImage = Resources.getImage("decrypt.png");
			viewImage = Resources.getImage("view.png");
			newFolderImage = Resources.getImage("new_folder.png");
		} catch(IOException ioe) {
			
		}
		
		backButton = createButton(backImage, "Назад");
		searchButton = createButton(searchImage, "Поиск");
		copyButton = createButton(copyImage, "Копировать");
		moveButton = createButton(moveImage, "Переместить");
		renameButton = createButton(renameImage, "Переименовать");
		deleteButton = createButton(deleteImage, "Удалить");
		archiveButton = createButton(archiveImage, "Упаковать в архив");
		unarchiveButton = createButton(unarchiveImage, "Распаковать архив");
		encryptButton = createButton(encryptImage, "Зашифровать");
		decryptButton = createButton(decryptImage, "Расшифровать");
		viewButton = createButton(viewImage, "Сменить вид");
		newFolderButton = createButton(newFolderImage, "Новая папка");
		
		try {
			backImage = Resources.getImage("back_disabled.png");
			searchImage = Resources.getImage("search_disabled.png");
			copyImage = Resources.getImage("copy_disabled.png");
			moveImage = Resources.getImage("move_disabled.png");
			renameImage = Resources.getImage("rename_disabled.png");
			deleteImage = Resources.getImage("delete_disabled.png");
			archiveImage = Resources.getImage("archive_disabled.png");
			unarchiveImage = Resources.getImage("unarchive_disabled.png");
			encryptImage = Resources.getImage("encrypt_disabled.png");
			decryptImage = Resources.getImage("decrypt_disabled.png");
			newFolderImage = Resources.getImage("new_folder_disabled.png");
			
			backButton.setDisabled(backImage);
			searchButton.setDisabled(searchImage);
			copyButton.setDisabled(copyImage);
			moveButton.setDisabled(moveImage);
			renameButton.setDisabled(renameImage);
			deleteButton.setDisabled(deleteImage);
			archiveButton.setDisabled(archiveImage);
			unarchiveButton.setDisabled(unarchiveImage);
			encryptButton.setDisabled(encryptImage);
			decryptButton.setDisabled(decryptImage);
			newFolderButton.setDisabled(newFolderImage);
		} catch(IOException ioe) {
			
		}
	}
	
	private void setToolsEnabled(boolean enabled) {
		backButton.setEnabled(enabled);
		searchButton.setEnabled(enabled);
		copyButton.setEnabled(enabled);
		moveButton.setEnabled(enabled);
		renameButton.setEnabled(enabled);
		deleteButton.setEnabled(enabled);
		archiveButton.setEnabled(enabled);
		unarchiveButton.setEnabled(enabled);
		encryptButton.setEnabled(enabled);
		decryptButton.setEnabled(enabled);
		newFolderButton.setEnabled(enabled);
	}
	
	public void disableTools() {
		setToolsEnabled(false);
	}
	
	public void enableTools() {
		setToolsEnabled(true);
	}
	
	private ImageView createButton(Image image, String tooltip) {
		ImageView view = new ImageView(image, TOOL_VIEW_SIZE, TOOL_VIEW_SIZE);
		view.setToolTipText(tooltip);
		view.addActionListener(this);
		
		add(view);
		
		return view;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		Tool tool;
		
		if(source.equals(backButton)) {
			tool = Tool.BACK;
		} else if(source.equals(searchButton)) {
			tool = Tool.SEARCH;
		} else if(source.equals(copyButton)) {
			tool = Tool.COPY;
		} else if(source.equals(moveButton)) {
			tool = Tool.MOVE;
		} else if(source.equals(deleteButton)) {
			tool = Tool.DELETE;
		} else if(source.equals(renameButton)) {
			tool = Tool.RENAME;
		} else if(source.equals(archiveButton)) {
			tool = Tool.ARCHIVE;
		} else if(source.equals(unarchiveButton)) {
			tool = Tool.UNARCHIVE;
		} else if(source.equals(encryptButton)) {
			tool = Tool.ENCRYPT;
		} else if(source.equals(decryptButton)) {
			tool = Tool.DECRYPT;
		} else if(source.equals(viewButton)) {
			tool = Tool.CHANGE_VIEW;
		} else if(source.equals(newFolderButton)) {
			tool = Tool.MKDIR;
		} else {
			return;
		}
		
		clickListener.onToolClick(tool);
	}
}

	
