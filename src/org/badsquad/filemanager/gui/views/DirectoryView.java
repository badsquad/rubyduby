package org.badsquad.filemanager.gui.views;

import java.awt.*;
import java.io.File;
import java.util.Collection;
import java.util.HashMap;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.badsquad.filemanager.gui.GUIListener;
import org.badsquad.filemanager.gui.models.DirectoryViewLayout;
import org.badsquad.filemanager.gui.models.FileViewMode;

public class DirectoryView extends JScrollPane {
	private static final long serialVersionUID = 5089303020610160695L;
	
	private GUIListener listener;
	private JPanel myPanel;
	private DirectoryViewLayout myLayout;
	
	private HashMap<File, FileView> files;
	
	public DirectoryView(GUIListener listener) {
		this.listener = listener;


		myPanel = new JPanel();
		myPanel.setBackground(new Color(162, 211, 238, 255));
		myLayout = new DirectoryViewLayout();
		myPanel.setLayout(myLayout);
		getViewport().add(myPanel);
		getVerticalScrollBar().setUnitIncrement(20);
		
		files = new HashMap<>();
	}
	
	public void removeAllFiles() {
		myPanel.removeAll();
		files.clear();
		repaint();
	}
	
	public void toggleViewMode() {
		FileViewMode mode = myLayout.getMode() == FileViewMode.ICONS ? FileViewMode.TABLE : FileViewMode.ICONS;
		myLayout.setMode(mode);
		Collection<FileView> views = files.values();
		for(FileView view : views) {
			view.setMode(mode);
		}
		updateUI();
	}
	
	public void addFile(File file) {
		FileView view = new FileView(file, listener);
		view.setMode(myLayout.getMode());
		myPanel.add(view);
		files.put(file, view);
		updateUI();
		repaint();
	}
	
	public void removeFile(File file) {
		FileView view = files.remove(file);
		remove(view);
	}
	
	public int getColumnCount() {
		return myLayout.getColumnCount();
	}
}
