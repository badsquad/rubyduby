package org.badsquad.filemanager.gui.views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.badsquad.filemanager.gui.GUIListener;
import org.badsquad.filemanager.gui.Constants;
import org.badsquad.filemanager.gui.models.FileViewMode;
import org.badsquad.filemanager.utils.FileUtils;
import org.badsquad.filemanager.utils.FontUtils;
import org.badsquad.filemanager.utils.Resources;

public class FileView extends JButton implements Constants, ActionListener, MouseListener {
	private static final long serialVersionUID = -3783838997515303033L;
	
	private GUIListener listener;
	
	private File file;
	private JPanel infoPanel;
	private JPanel iconPanel;
	private ImageView bigIcon;
	private ImageView smallIcon;
	private JCheckBox checkBox;
	private JLabel name;
	private JLabel type;
	private JLabel sizeAndDate;
	
	public FileView(File file, GUIListener listener) {
		this.listener = listener;
		
		setBorder(null);
		setBackground(new Color(0, 0, 0, 0));
		Insets insets = getInsets();
		insets.left = insets.right = insets.top = insets.bottom = 0;
		
		this.file = file;
		
		setLayout(new BorderLayout());
		
		Image bigImage = null, smallImage = null;
		try {
			if(file.isFile()) {
				String[] mime = FileUtils.getFileMimeType(file);
				if(mime != null && mime[0].equals("image")) {
					bigImage = ImageIO.read(file);
				} else {
					bigImage = createCombinedIcon(ICON_FILE_VIEW_HEIGHT);
				}
				smallImage = createCombinedIcon(TABLE_FILE_VIEW_HEIGHT);
			} else {
				smallImage = bigImage = Resources.getImage("folder.png");
			}
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
		
		infoPanel = new JPanel();
		infoPanel.setBackground(new Color(162, 211, 238, 255));
		infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
		
		iconPanel = new JPanel();
		iconPanel.setBackground(new Color(162, 211, 238, 255));
		iconPanel.setBackground(new Color(162, 211, 238, 255));
		iconPanel.setLayout(new FlowLayout());
		
		bigIcon = new ImageView(bigImage, ICON_FILE_VIEW_HEIGHT-50, ICON_FILE_VIEW_HEIGHT-50);
		bigIcon.setBackground(new Color(162, 211, 238, 255));
		bigIcon.addMouseListener(this);
		
		smallIcon = new ImageView(smallImage, TABLE_FILE_VIEW_HEIGHT, TABLE_FILE_VIEW_HEIGHT);
		smallIcon.setBackground(new Color(162, 211, 238, 255));
		smallIcon.addMouseListener(this);
		iconPanel.add(smallIcon);
		
		checkBox = new JCheckBox();
		checkBox.setBackground(new Color(176, 224, 230, 255));
		checkBox.addActionListener(this);
		
		name = new JLabel(FileUtils.getSystemName(file));
		name.setBackground(new Color(176, 224, 230, 255));
		FontUtils.setFontSize(name, 1.33f);
		FontUtils.setFontStyle(name, Font.PLAIN);
		
		sizeAndDate = new JLabel();
		setBackground(new Color(162, 211, 238, 255));
		FontUtils.setFontSize(sizeAndDate, 0.9f);
		FontUtils.setFontStyle(sizeAndDate, Font.ITALIC);
		String text = FileUtils.stringifyLastModified(file);
		infoPanel.add(sizeAndDate);
		
		if(file.isFile()) {
			text = FileUtils.stringifySize(file) + " | " + text;
		}
		sizeAndDate.setText(text);
		
		type = new JLabel();
		type.setBackground(new Color(238, 238, 238));
		FontUtils.setFontSize(type, 0.9f);
		FontUtils.setFontStyle(type, Font.ITALIC);
		infoPanel.add(type);
		
		if(file.isFile()) {
			type.setText(FileUtils.localeFileType(file));
		}
		
		setMode(FileViewMode.TABLE);
		addMouseListener(this);
	}
	
	public void setMode(FileViewMode mode) {
		removeAll();
		if(mode == FileViewMode.ICONS) {
			infoPanel.remove(name);
			iconPanel.remove(checkBox);
			
			add(bigIcon, BorderLayout.NORTH);
			add(name, BorderLayout.CENTER);
			add(checkBox, BorderLayout.SOUTH);
		} else {
			add(iconPanel, BorderLayout.WEST);
			infoPanel.add(name, 0);
			add(infoPanel, BorderLayout.CENTER);
			iconPanel.add(checkBox, 0);
		}
		repaint();
	}
	
	private Image createCombinedIcon(int size) throws IOException {
		Image image = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
		
		Image icon = Resources.getImage("file.png");
		icon = icon.getScaledInstance(size, size, Image.SCALE_AREA_AVERAGING);
		
		Graphics graphics = image.getGraphics();
		graphics.drawImage(icon, 0, 0, null);
		
		Icon systemIcon = FileUtils.getSystemIcon(file);
		int inset = (size - systemIcon.getIconWidth()) / 2; 
		systemIcon.paintIcon(this, graphics, inset, inset);
		
		graphics.dispose();
		return image;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if(source.equals(checkBox)) {
			if(checkBox.isSelected()) {
				listener.onFileSelect(file);
			} else {
				listener.onFileDeselect(file);
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if(e.getClickCount() == 2) {
			listener.onFileClick(file);
		} else {
			if(checkBox.isSelected()) {
				listener.onFileDeselect(file);
			} else {
				listener.onFileSelect(file);
			}
			checkBox.setSelected(!checkBox.isSelected());
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}
}
