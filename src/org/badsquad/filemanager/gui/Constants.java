package org.badsquad.filemanager.gui;

public interface Constants {
	public static final int TABLE_FILE_VIEW_WIDTH = 200;
	public static final int TABLE_FILE_VIEW_HEIGHT = 50;
	public static final int ICON_FILE_VIEW_WIDTH = 125;
	public static final int ICON_FILE_VIEW_HEIGHT = 125;
	public static final int TOOL_VIEW_SIZE = 50;
}
