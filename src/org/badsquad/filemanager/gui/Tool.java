package org.badsquad.filemanager.gui;

public enum Tool {
	BACK, SEARCH, COPY, MOVE, RENAME, DELETE, ARCHIVE, UNARCHIVE, ENCRYPT, DECRYPT, CHANGE_VIEW, MKDIR
}