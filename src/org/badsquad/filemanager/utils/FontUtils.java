package org.badsquad.filemanager.utils;

import java.awt.Font;

import javax.swing.JComponent;

public class FontUtils {
	public static void setFontSize(JComponent component, float size) {
		Font font = component.getFont();
		component.setFont(font.deriveFont(font.getSize() * size));
	}
	
	public static void setFontStyle(JComponent component, int style) {
		Font font = component.getFont();
		component.setFont(font.deriveFont(style));
	}
}
