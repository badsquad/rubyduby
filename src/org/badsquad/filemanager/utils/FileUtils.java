package org.badsquad.filemanager.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.swing.Icon;
import javax.swing.filechooser.FileSystemView;

public class FileUtils {
	private static HashMap<String, String> knownTypes = new HashMap<>();
	private static FileSystemView fileSystem = FileSystemView.getFileSystemView();
	
	static {
	    //известные типы файлов
	    
		knownTypes.put("audio", "Звуковая дорожка");
		knownTypes.put("video", "Видео");
		knownTypes.put("image", "Изображение");
		knownTypes.put("text", "Текстовый документ");
	}
	
	public static String[] getFileMimeType(File file) throws IOException {
		String mime = Files.probeContentType(file.toPath());
		if(mime == null) 
			return null;
		else
			return mime.split("/");
	}
	
	/**
	 * Возвращает человеко-понятный тип файла
	 * 
	 * @param file файл
	 * @return тип файла
	 */
	public static String localeFileType(File file) {
		try {
		    //mime-тип файла
			String[] mime = getFileMimeType(file);
			//если системе известен тип
			if(mime != null) {
				//отладка
			    System.err.println(mime[0] + "/" + mime[1]);
				
			    //группа типов
				String group = mime[0];
				//конкретно тип
				String type = mime[1];
				
				//если тип принадлежит группе приложений
				if(group.equals("application")) {
				    //разбор типов
					int index = type.lastIndexOf(".");
					if(index > -1) {
						switch(type.substring(index+1)) {
							case "document": return "Документ";
							case "sheet": return "Таблица";
							case "presentation": return "Презентация";
						}
					}
					if(type.contains("zip")) {
						return "ZIP-Архив";
					}
					switch(type) {
						case "pdf": return "PDF-файл"; 
						case "msaccess": return "База данных MS Access";
					}
				}
				
				//поиск среди известных типов
				String message = knownTypes.get(group);
				if(message != null) {
					return message;
				} else {
					String name = file.getName();
					return name.substring(name.lastIndexOf(".")) + "-файл";
				}
			}
		} catch(IOException ioe) {
			
		}
		return "Файл";
	}

	/**
	 * Вовзвращает иконку, ассоциированную с файлом в системе
	 * 
	 * @param file файл
	 * @return иконка, ассоциированная с файлом в системе
	 */
	public static Icon getSystemIcon(File file) {
		return fileSystem.getSystemIcon(file);
	}
	
	/**
	 * Возвращает имя файла в ОС
	 * 
	 * @param file файл
	 * @return имя файла в ОС
	 */
	public static String getSystemName(File file) {
		String name = fileSystem.getSystemDisplayName(file);
		if(name.isEmpty()) {
			name = file.getName();
			if(name.isEmpty()) {
				name = file.getPath();
			}
		}
		return name;
	}
	
	/* размерности файлов */
	private static String[] fileSizeDigits = new String[]{ 
		"байт", "КБ", "МБ", "ГБ", "ТБ"	
	};
	
	/**
	 * Преобразовывает размер файла в байтах в человеко-читабельный вид
	 * 
	 * @param file файл
	 * @return размер файла в нужной величине
	 */
	public static String stringifySize(File file) {
		if(file.isFile()) {
			long bytes = file.length();
			int current = 0;
			
			while(true) {
				double div = Math.pow(1024, current+1);
				if(bytes / div < 1) {
					break;
				}
				current++;
			}
			
			return String.format("%d %s", bytes / (int)Math.pow(1024, current), fileSizeDigits[current]);
		}
		
		//информация о папке, кол-во файлов в ней		
		File[] files = file.listFiles();
		if(files == null || files.length == 0) {
			return "Пустая папка";
		}
		
		//правильное склонение
		int count = files.length;
		String end = "";
		if(count % 10 >= 5 || (count > 10 && count < 15)) {
			end = "ов";
		} else if(count % 10 > 1 && count % 10 < 5) {
			end = "а";
		}
		return count + " файл" + end;
	}
	
	/**
	 * Преобразовывает информацию о дате последнего изменения файла в человеко-понимаемый вид
	 * 
	 * @param file файл
	 * @return строка вида ДД-ММ-ГГГГ чч-мм-сс, где чч - часы, мм- минуты, сс - секунды
	 */
	public static String stringifyLastModified(File file) {
		Date date = new Date(file.lastModified());
		DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		return format.format(date);
	}

}
