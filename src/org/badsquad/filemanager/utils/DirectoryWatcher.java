package org.badsquad.filemanager.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import org.badsquad.filemanager.gui.GUIListener;

public class DirectoryWatcher extends Thread {
	private GUIListener listener;
	private Path directory;
	private FileSystem fileSystem;
	private WatchService service;
	private boolean work = true;
	
	public DirectoryWatcher(GUIListener listener) {
		this.listener = listener;
	}
	 
	public void setDirectory(File dir) {
		if(dir == null) {
			if(service != null) {
				try {
					service.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return;
		}
		
		directory = dir.toPath();
		fileSystem = directory.getFileSystem();
		try {
			WatchService wservice = fileSystem.newWatchService();
			this.service = wservice;
			directory.register(service, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE);
		} catch(IOException ioe) {
			
		}
	}
	
	public void kill() {
		work = false;
	}
	
	public void run() {
		while (work) {
			if(service != null) {
				WatchKey key;
	            try {
	            	key = service.take();
	            	
					for(WatchEvent<?> watchEvent : key.pollEvents()) {
						Kind<?> kind = watchEvent.kind();
					    
						Path path = (Path)(watchEvent.context());
					    if(kind == StandardWatchEventKinds.ENTRY_CREATE) {
					    	listener.onFileCreated(path.toFile());
					    } else if(kind == StandardWatchEventKinds.ENTRY_DELETE) {
					    	listener.onFileDeleted(path.toFile());
					    }
					}
					 
					if(!key.reset()) {
						break;
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
        }
	}
}
