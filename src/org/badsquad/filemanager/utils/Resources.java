package org.badsquad.filemanager.utils;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

public class Resources {
	public static final boolean EXPORTED = false;
	
	public static Image getImage(String name) throws IOException {
		if(EXPORTED) {
			URL res = Resources.class.getClassLoader().getResource("res/img/" + name);
			return ImageIO.read(res);
		} else {
			File res = new File("res/img/", name);
			return ImageIO.read(res);	
		}  
	}
}
