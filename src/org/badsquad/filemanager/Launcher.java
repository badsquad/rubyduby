package org.badsquad.filemanager;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import org.badsquad.filemanager.gui.MainFrame;

public class Launcher {

	public static void main(String[] args) {
		/*try {
			System.setErr(new PrintStream(new FileOutputStream("error.log")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}*/	
		
		MainFrame frame = new MainFrame();
		frame.setVisible(true);
	}

}
